﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DroneSystem.Models.Database
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {}
        public DbSet<User> Users { get; set; }
        public DbSet<Field> Field { get; set; }
        public DbSet<Drone> Drone { get; set; }
        public DbSet<DockingStation> DockingStation { get; set; }
        public DbSet<Survey> Survey { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Survey>()
                .HasKey(c => new { c.date, c.field_id });
            
        }
    }
}
