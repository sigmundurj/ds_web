﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DroneSystem.Models.Database
{
    public class Survey
    {
     
        public DateTime date { get; set; }
        public int field_id { get; set; }
        [ConcurrencyCheck]
        public string data { get; set; }
        [ConcurrencyCheck]
        public string status { get; set; }
  
    }
}
