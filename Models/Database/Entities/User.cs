﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DroneSystem.Models.Database
{
        public class User
        {
            [Key]
            public string email { get; set; }
            public string name { get; set; }
            public int is_admin { get; set; }
            public string password { get; set; }
        }
}
