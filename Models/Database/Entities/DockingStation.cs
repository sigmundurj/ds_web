﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DroneSystem.Models.Database
{
    public class DockingStation
    {
        [Key]
        public int? ds_id { get; set; }
        
        public string ssid { get; set; }
    }
}
