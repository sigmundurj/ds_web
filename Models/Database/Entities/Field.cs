﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DroneSystem.Models.Database
{
    public class Field
    {
        [Key]
        public int? field_id { get; set; }
        public string email { get; set; }
        public int? ds_id { get; set; }
        public string coordinates { get; set; }
    }
}
