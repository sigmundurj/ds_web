﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DroneSystem.Models.Database
{
    public class Drone
    {
        [Key]
        public string ssid { get; set; }
        public string email { get; set; }
    }
}
