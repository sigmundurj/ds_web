﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DroneSystem.Models.Auth
{
    public class AdminRole : IAuthorizationRequirement
    {
        public AdminRole(int admin)
        {
            role = admin;
        }

        public int role { get; set; }
    }

    public class AdminRoleHandler : AuthorizationHandler<AdminRole>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdminRole requirement)
        {
            if (context.User.HasClaim(c => c.Value == "Admin"))
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
