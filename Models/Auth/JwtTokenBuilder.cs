﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DroneSystem.Models.Auth
{
    public class JwtTokenBuilder
    {
        public string Build(string name, string email, int admin)
        {
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("verysecretkeylinuxftw"));

            var claims = new Claim[] {
                new Claim(ClaimTypes.Name, name),
                new Claim(JwtRegisteredClaimNames.Email, email)
            };
            string role = "User";
            role = admin == 1 ? "Admin" : "User";

            var token = new JwtSecurityToken(
                issuer: "DroneSystem",
                audience: role,
                claims: claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256)
            );
            string jwtToken = new JwtSecurityTokenHandler().WriteToken(token);
            return jwtToken;
        }
    }
}
