﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DroneSystem.Models.Auth
{
    public class UserAuthorized : IAuthorizationRequirement
    {
        public UserAuthorized()
        {

        }
        public int role { get; set; }
    }

    public class UserAuthorizedHandler : AuthorizationHandler<UserAuthorized>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserAuthorized requirement)
        {
            if (context.User.HasClaim(c => c.Value == "User") )
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
