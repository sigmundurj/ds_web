﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DroneSystem.Models.Auth
{
    public class UserNotAuthorized : IAuthorizationRequirement
    {
        public UserNotAuthorized()
        {

        }
        public int role { get; set; }
    }

    public class UserNotAuthorizedHandler : AuthorizationHandler<UserNotAuthorized>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, UserNotAuthorized requirement)
        {
            if (context.User.Claims.Count() == 0)
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}
