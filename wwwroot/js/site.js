﻿// Write your JavaScript code.

var map;
let settings =  {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 12
};

let map_style = { draggable: true, editable: true };
let geoJson;

jQuery(document).ready(function ($) {
    $('[data-action^="StartSurvey"]').on('click', function () {
        let id = parseInt($(this).attr('data-action').replace('StartSurvey-', ''));
        
        $.ajax({
            type: "POST",
            url: "/Account/MyFields",
            data: 'id='+id,
            async: true,
            success: function (msg) {
                console.log(msg);
                window.location.href = window.location.href;
            },
            error: function () {
                window.location.href = window.location.href;
                return "error";      
            }
        });
    });

    $('[data-action^="DeleteDrone"]').on('click', function () {
        let id = $(this).attr('data-action').replace('DeleteDrone-', '');

        $.ajax({
            type: "POST",
            url: "/Admin/DeleteDrone",
            data: 'id=' + id,
            async: true,
            success: function (msg) {
                
                window.location.href = window.location.href;
                $('h2').append(msg);

            },
            error: function () {
                window.location.href = window.location.href;
                return "error";
            }
        });
    });

    $('[data-action^="DeleteFields"]').on('click', function () {
        let id = parseInt($(this).attr('data-action').replace('DeleteFields-', ''));

        $.ajax({
            type: "POST",
            url: "/Admin/DeleteField",
            data: 'id=' + id,
            async: true,
            success: function (msg) {

                window.location.href = window.location.href;
                $('h2').append(msg);

            },
            error: function () {
                window.location.href = window.location.href;
                return "error";
            }
        });
    });

    $('#addrone_form #email').on('focusout', function () {
        let email = $(this).val();
        
        $.ajax({
            type: "POST",
            url: "/Admin/GetDS",
            data: 'email=' + email,
            async: true,
            success: function (msg) {
                
                msg = JSON.parse(msg);
                let filtered = msg.filter(ms => {
                    return ms !== null
                })
                let options = ""
                filtered.forEach(elem => {
                    options += '<option value="'+ elem +'">'+elem+'</option>';
                })
                let def = $('#dsid').html();
                $('#dsid').empty().append(def + options);

            },
            error: function () {
                
                return "error";
            }
        });
    });


    $('#addfield_form').on('submit', function (e) {
        let coordinates;
        map.data.toGeoJson(function (o) {
            if (o.features[0] !== undefined) {
                coordinates = o.features[0].geometry.coordinates[0];
            }
        });    
        if (coordinates === undefined) {
            $('em').empty().append('Please select the field on the map!');
            e.preventDefault();
        }
        else {
            $('#addfield_form').find('[type="hidden"]').val(JSON.stringify(coordinates));
        }      
    });

    if ($('#field_coordinates').length === 1) {
        let coordinates = $('#field_coordinates').val()
        settings = {
            center: { lat: JSON.parse(coordinates)[0][1], lng: JSON.parse(coordinates)[0][0] },
            zoom: 8
        };
        map_style =  { draggable: false, editable: false }; 
        
        geoJson = {
            "type": "Feature",
            "geometry": {
                "type": "Polygon",
                "coordinates": [JSON.parse(coordinates)]
            },
            "properties": {
                
            }
        }
    }

});




function initMap() {
    map = new google.maps.Map(document.getElementById('map'), settings);
        
    map.data.setStyle(map_style);
    map.data.setControls(['Polygon']);
    map.data.addGeoJson(geoJson);
}


