﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using DroneSystem.Models;
using Microsoft.AspNetCore.Authorization;
using DroneSystem.Models.Database;
using DroneSystem.Models.Auth;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using System.IO;
using System.Drawing;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace DroneSystem.Controllers
{
    public class DroneController : Controller
    {
        private DatabaseContext db;
        private IHostingEnvironment env;
        public DroneController(DatabaseContext db, IHostingEnvironment env)
        {
            this.db = db;
            this.env = env;
        }

        [HttpGet]
        [Authorize]
        public String Test()
        {
            var claims = User.Claims;
            var headers = ControllerContext.HttpContext.Request.Headers.ToString();

            return "Success!";
        }

        public async Task<string> Test_PostAsync()
        {
            var payload = new User
            {
                email = "asd",
                name = "asd",
                is_admin = 1,
                password = "asd"

            };
            var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(payload));
            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            using (var client = new HttpClient())
            {
                HttpResponseMessage res = await client.PostAsync("http://localhost:3000/", httpContent);
                return await res.Content.ReadAsStringAsync();
            }

        }
        public IActionResult CreateCookie(String email, String password)
        {
            
            //var token = new JwtTokenBuilder()  
            //            .Build();
            //Response.Cookies.Append("token", token);
            return View();//new ObjectResult(token);

        }

        [HttpGet]
        public string getID()
        {
            DockingStation ds = new DockingStation { ds_id = null, ssid = null };

            db.DockingStation.Add(ds);
            db.SaveChanges();

            var query = from e in db.DockingStation
                        select e;

            return query.ToList().Last().ds_id.ToString(); 
        }

        [HttpGet]
        public string SurveyAsync(int ds_id)
        {
            var field = (from e in db.Field
                        where e.ds_id == ds_id
                        select e).First();
            var survey = (from e in db.Survey
                          where e.field_id == field.field_id && e.status == "Pending"
                          select e).ToList().Count;

            if(survey == 0)
            {
                return "";
            }
            db.SaveChanges();
            return JsonConvert.SerializeObject(field.coordinates);
        }

        [HttpPost]
        public string PostResult(int ds_i, string dat)
        {
            var field = (from e in db.Field
                        where e.ds_id == ds_i
                        select e).First();
            db.SaveChanges();
            var survey = (from e in db.Survey
                          where e.field_id == field.field_id && e.status == "Pending"
                          select e).First();
            db.SaveChanges();
            
           
            var rel_path = "\\Survey\\" + survey.field_id.ToString();
            if (!System.IO.Directory.Exists((env.WebRootPath) + rel_path))
            {
                System.IO.Directory.CreateDirectory((env.WebRootPath) + rel_path);
            }
            rel_path += "\\" + survey.date.Ticks + ".jpg";

            var result = db.Survey.SingleOrDefault(e => e.field_id == survey.field_id && e.status == "Pending" );
            result.status = "Finished";
            result.data = rel_path;
            try

            {

                db.SaveChanges();

            }

            catch (DbUpdateConcurrencyException ex)

            {

                var d = ex.Entries.Single();
                d.Reload();


                db.SaveChanges();

            }

            var converted_data = Convert.FromBase64String(dat.Replace("data:image/jpg;base64,", ""));

            MemoryStream ms = new MemoryStream(converted_data, 0, converted_data.Length);
            ms.Write(converted_data, 0, converted_data.Length);
            Image image = Image.FromStream(ms, true);

           
            
            image.Save((env.WebRootPath) + rel_path, System.Drawing.Imaging.ImageFormat.Png);

           


            return "OK";
        }
    }
}