﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DroneSystem.Models.Database;
using DroneSystem.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Authorization;

namespace DroneSystem.Controllers
{
    public class AccountController : Controller
    {
        private DatabaseContext db;
        public AccountController(DatabaseContext db)
        {
            this.db = db;
        }
        [Authorize(Policy = "User")]
        [HttpPost]
        public IActionResult MyFields(int id)
        {
            Survey s = new Survey() { date = DateTime.Now, field_id = id, status = "Pending" };
            db.Survey.Add(s);
            db.SaveChanges();
            return View();
        }
        [Authorize(Policy="User")]
        public IActionResult MyFields()
        {
            var email = HttpContext.User.Claims.AsEnumerable().ElementAt(1).Value;
            var query = from e in db.Field
                        where e.email == email
                        orderby e.email
                        select e;
            var surveys = (from e in db.Survey
                          select e).ToList();
            List<Field> fields = query.ToList();
            ViewData["Title"] = "Here is a list of all your fields.";
            ViewData["Fields"] = fields;
            ViewData["Surveys"] = surveys;
            return View();
        }
        [Authorize(Policy = "User")]
        public IActionResult YourLogs()
        {
            var query = (from e in db.Survey
                        where e.status == "Finished"
                        select e).ToList();
            foreach(Survey x in query)
            {
                x.data.Replace("\\", "/");
            }
            ViewData["Title"] = "Here is a list of all your fields.";
            ViewData["Surveys"] = query;
            return View();
        }
        [Authorize(Policy = "User")]
        public IActionResult MyProfile()
        {
            ViewData["Message"] = "My Profile";
            return View();
        }
        [Authorize(Policy = "User")]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [Authorize(Policy = "User")]
        public IActionResult Map(int id)
        {
            var query = from e in db.Field
                        where e.field_id == id
                        select e;
            Field field = query.ToList().First();
            ViewData["Field"] = field;
            return View();
        }
        [Authorize(Policy = "User")]
        [HttpPost]
        public IActionResult EditProfile(string name, string password)
        {
            var email = HttpContext.User.Claims.AsEnumerable().ElementAt(1).Value;
            var result = db.Users.SingleOrDefault(e => e.email == email);
            if(result != null)
            {
                result.name = name;
                result.password = password;
                db.SaveChanges();
                ViewData["Success"] = "Your profile has been updated!";
            }
            else
            {
                ViewData["Error"] = "Something went wrong!";
            }
            return View();
        }
        [Authorize(Policy = "User")]
        public IActionResult EditProfile()
        {
            return View();
        }
    }
}