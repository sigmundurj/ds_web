﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DroneSystem.Models.Database;
using Newtonsoft.Json;

namespace DroneSystem.Controllers
{
    public class AdminController : Controller
    {
        private DatabaseContext db;
        public AdminController(DatabaseContext db)
        {
            this.db = db;
        }
        //AddField - Use Case
        [HttpPost]
        [Authorize(Policy = "AdminRole")]
        public IActionResult AddField(string email,int dsid, string coordinates)
        {
            var query = from e in db.Users
                        where e.email == email
                        orderby e.email
                        select e;

            var myusers = query.ToList();
            if(myusers.Count == 1)
            {
                Field field = new Field() {field_id=null, email = email, ds_id=dsid, coordinates = coordinates};
                db.Field.Add(field);
                db.SaveChanges();
                ViewData["Message"] = "The field has been registered.";
                var q = (from e in db.DockingStation
                            where !db.Field.Any(f => f.ds_id == e.ds_id)
                            select e.ds_id).ToList();
                ViewData["ids"] = q;
                return View();
            }

            var list = (from e in db.DockingStation
                        where !db.Field.Any(f => f.ds_id == e.ds_id)
                        select e.ds_id).ToList();
            ViewData["ids"] = list;
            ViewData["Error"] = "There is no user with the specified email!";
            return View();
           
        }
        [Authorize(Policy = "AdminRole")]
        public IActionResult AddField()
        {
            var list = (from e in db.DockingStation
                        where !db.Field.Any(f => f.ds_id == e.ds_id)
                        select e.ds_id).ToList();
            ViewData["ids"] = list;

            return View();
        }

        [HttpPost]
        [Authorize(Policy = "AdminRole")]
        public IActionResult AddDrone(string email, string ssid, int dsid)
        {
            var query = from e in db.Users
                        where e.email == email
                        orderby e.email
                        select e;

            var myusers = query.ToList();
            if (myusers.Count == 1)
            {
                var drones = from e in db.Drone
                            where e.ssid == ssid
                            select e;
                if(drones.ToList().Count == 0)
                {
                    db.Drone.Add(new Drone() { ssid = ssid, email = email });
                    var result = db.DockingStation.SingleOrDefault(e => e.ds_id == dsid);
                    if (result != null)
                    {
                        result.ssid = ssid;
                        db.SaveChanges();
                    }
                    db.SaveChanges();
                    ViewData["Success"] = "Drone was added successfully!";
                }
                else
                {
                    ViewData["Error"] = "There already exists a drone with the specified ssid!";
                }
                return View();
            }

            ViewData["Error"] = "There is no user with the specified email!";
            return View();

        }
        [HttpPost]
        [Authorize(Policy = "AdminRole")]
        public string GetDS(string email)
        {
            var list = (from e in db.Field
                        where db.DockingStation.Any(f => (f.ds_id == e.ds_id && f.ssid == null))
                        select e.ds_id).ToList();
            return JsonConvert.SerializeObject(list);
        }

        public IActionResult AddDrone()
        {
            return View();
        }


        public IActionResult ViewDrones()
        {
            var drones = (from e in db.Drone
                         select e).ToList();
            ViewData["Drones"] = drones;
            return View();
        }

        [HttpPost]
        [Authorize(Policy = "AdminRole")]
        public string DeleteDrone(string id)
        {
            var drone = (from e in db.Drone
                         where e.ssid == id
                          select e).ToList();
            
            db.Drone.Remove(drone.First());
            db.SaveChanges();
            return "Success";
        }

        public IActionResult ViewFields()
        {
            var fields = (from e in db.Field
                          select e).ToList();
            ViewData["Fields"] = fields;
            return View();
        }

        [HttpPost]
        [Authorize(Policy = "AdminRole")]
        public string DeleteField(int id)
        {
            var field = (from e in db.Field
                         where e.field_id == id
                         select e).ToList();

            db.Field.Remove(field.First());
            db.SaveChanges();
            return "Success";
        }
    }
}