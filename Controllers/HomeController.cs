﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using DroneSystem.Models;
using Microsoft.AspNetCore.Authorization;
using DroneSystem.Models.Database;
using DroneSystem.Models.Auth;

namespace DroneSystem.Controllers
{
    public class HomeController : Controller
    {
        private DatabaseContext db;
        public HomeController(DatabaseContext db)
        {
            this.db = db;
        }

        [HttpGet("/")]
        public IActionResult Index()
        {
            ViewBag.Message = "Hello!";
            ViewBag.Time = DateTime.Now;


            var employees = from e in db.Users
                            orderby e.email
                            select e;
            return View(employees);
        }

        public IActionResult About(string name, int num)
        {

            ViewData["Message"] = "Your application description page.";
            ViewData["Data"] = name + num;
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public new IActionResult User()
        {
            List<User> userlist = new List<User>();
            var query = from e in db.Users
                        where e.email == "admin@dronesystem.com"
                        orderby e.email
                        select e;

            var myusers = query.ToList();
            return View();
        }
        [HttpPost]
        public IActionResult Login(String email, String password)
        {
            var query = from e in db.Users
                        where e.email == email
                        where e.password == password
                        orderby e.email
                        select e;
            var myusers = query.ToList();

            if (myusers.Count != 0)
            {
                var token = new JwtTokenBuilder()
                                        .Build(myusers[0].name, email, myusers[0].is_admin);
                Response.Cookies.Append("token", token);
                RedirectToActionResult redirectResult = new RedirectToActionResult("Index", "Home", new { });
                return redirectResult;
            }
            else
            {
                ViewData["Error"] = "User or password are not correct!";
                return View();
            }
        }

        [Authorize(Policy = "NotLogged")]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(String email, String password, String name, bool adminp)
        {
            //check in db if email exists
            var query = from e in db.Users
                        where e.email == email
                        orderby e.email
                        select e;
            var myusers = query.ToList();

            if (myusers.Count != 0)
            {
                ViewData["Error"] = "User already exists";
                return View();
            }
            var admin = adminp ? 1 : 0;
            User u = new User { email = email, name = name, is_admin = admin, password = password };
            db.Users.Add(u);
            db.SaveChanges();

            ViewData["Message"] = "User has been registered.";
            return View("Success");
        }
        [Authorize(Policy = "AdminRole")]
        public IActionResult Register()
        {
            return View();
        }

        [Authorize(Policy = "User")]
        public IActionResult Logout()
        {
            RedirectToActionResult redirectResult = new RedirectToActionResult("Index", "Home", new { });
            return redirectResult;
        }
    }
}
